if (document.getElementById('map-canvas')){
    
    var directionsService = new google.maps.DirectionsService;
    var directionsDisplay = new google.maps.DirectionsRenderer({
        suppressMarkers: true
    });
 
    var first_place     = new google.maps.LatLng(41.878,-87.629);
    var second_place    = new google.maps.LatLng(40.714,-74.005);
    var third_place     = new google.maps.LatLng(34.052,-118.243);
    var fourth_place    = new google.maps.LatLng(35.052,-118.243);

    var places = [];
    places[0] = first_place;
    places[1] = second_place;
    places[2] = third_place;
    places[3] = fourth_place;

    var mapOptions = {
        zoom: 4,
        center: first_place,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);

    var vektor=[];

    var circlesArray = new Array();

    var markersArray = new Array();
    var marker;
    for(var i = 0; i < places.length; i++){

        marker = new google.maps.Marker({
            position: places[i],
            map: map,
        });
        markersArray.push(marker);

        circle = new google.maps.Circle({
            strokeColor: '#FF0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#FF0000',
            fillOpacity: 0.35,
            map: map,
            center: places[i],
            radius: 50*1350
        });
          
        circlesArray.push(circle);

        var infowindow = new google.maps.InfoWindow();
        google.maps.event.addListener(marker, "click", (function(marker) {
        return function(evt) {
            if(vektor.length == 0)
                vektor.push(marker.getPosition());
            else if(vektor.length == 1){
                vektor.push(marker.getPosition());
                calculateAndDisplayRoute(directionsService, directionsDisplay); 
            } else {
                directionsDisplay.set('directions', null);
                vektor = [];
                vektor.push(marker.getPosition());
            }
        }
        })(marker));
    }

    function getDirections(circle){
        console.log("Gjatesia: " + vektor.length);
        if(vektor.length == 0)
              vektor.push(circle.getCenter());
        else if(vektor.length == 1){
            vektor.push(circle.getCenter());
            calculateAndDisplayRoute(directionsService, directionsDisplay); 
        } else {
            directionsDisplay.set('directions', null);
            vektor = [];
            vektor.push(circle.getCenter());
        }
    }

    directionsDisplay.setMap(map);

    function calculateAndDisplayRoute(directionsService, directionsDisplay) {
        directionsService.route({
            origin: vektor[0],
            destination: vektor[1],
            travelMode: 'WALKING'
        }, function(response, status) {
            if (status === 'OK') {
            directionsDisplay.setDirections(response);
            } else {
            window.alert('Directions request failed due to ' + status);
            }
        });
    }
}
