// if HTML DOM Element that contains the map is found...

if (document.getElementById("map-canvas")) {
  var count = 0;
  // Coordinates to center the map
  var myLatlng = new google.maps.LatLng(41.324665368, 19.818663392);
  var dest = new google.maps.LatLng(41.33, 19.82);
  // Other options for the map, pretty much selfexplanatory
  var mapOptions = {
    zoom: 15,
    center: myLatlng,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  var map = new google.maps.Map(
    document.getElementById("map-canvas"),
    mapOptions
  );

  var marker = new google.maps.Marker({
    position: myLatlng,
    map: map,
    title: "Tirana"
  });
  var dest = new google.maps.Marker({
    position: dest,
    map: map,
    title: "Dest"
  });

  poly = new google.maps.Polyline({
    strokeColor: "#FFFF00",
    strokeOpacity: 1.0,
    strokeWeight: 2.5
  });
  poly.setMap(map);
  map.addListener("click", addLatLng);
}
var path = poly.getPath();
path.push(myLatlng);
function addLatLng(event) {
  path.push(event.latLng);
  addCircle(event);

  function addCircle(event) {
    count++;
    console.log("event", event, count);
    var circle = new google.maps.Circle({
      strokeColor: "#FF0000",
      strokeOpacity: 1,
      strokeWeight: 2,
      fillColor: "#FF0000",
      fillOpacity: 1,
      map: map,
      center: event.latLng,
      radius: count
    });
    console.log(circle);
  }
}
