if (document.getElementById("map-canvas")) {
  var myLatlng = new google.maps.LatLng(41.324665368, 19.818663392);
  var mapOptions = {
    zoom: 15,
    center: myLatlng,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    disableDoubleClickZoom: true
  };
  var map = new google.maps.Map(
    document.getElementById("map-canvas"),
    mapOptions
  );
  var coordinates = new google.maps.MVCArray();
  coordinates.push(myLatlng);
  var polygon = new google.maps.Polygon({
    paths: coordinates,
    strokeColor: "#FF0000",
    strokeOpacity: 0.8,
    strokeWeight: 2,
    fillColor: "#FF0000",
    fillOpacity: 0.35
  });
  polygon.setMap(map);
  google.maps.event.addListener(map, "dblclick", function(event) {
    buildPolygon(event.latLng);
  });
  function buildPolygon(pnt) {
    if (coordinates.length == 5) {
      coordinates.removeAt(0);
      coordinates.push(new google.maps.LatLng(pnt.lat(), pnt.lng()));
    } else {
      coordinates.push(new google.maps.LatLng(pnt.lat(), pnt.lng()));
    }
  }
}
