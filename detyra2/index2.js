
if (document.getElementById("map-canvas")) {
  var myLatlng = new google.maps.LatLng(41.324665368, 19.818663392);
  var dest = new google.maps.LatLng(41.33, 19.82);
  var mapOptions = {
    zoom: 15,
    center: myLatlng,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  var map = new google.maps.Map(
    document.getElementById("map-canvas"),
    mapOptions
  );

  var marker = new google.maps.Marker({
    position: myLatlng,
    map: map,
    title: "Tirana"
  });
  var dest = new google.maps.Marker({
    position: dest,
    map: map,
    title: "Dest"
  });

  poly = new google.maps.Polyline({
    strokeColor: "#FFFF00",
    strokeOpacity: 1.0,
    strokeWeight: 1.5
  });

  poly.setMap(map);
  map.addListener("click", addLatLng);
}
var path = poly.getPath();

path.push(myLatlng);
path.push(dest.position);

function addLatLng(event) {
  path.pop();
  path.push(event.latLng);
  path.push(dest.position);
}
